import React, { Component } from 'react';
import './App.css';
import MyCars from './components/MyCars';

class App extends Component {

    state = {
      'titre':'Mon catalogue de voitures'
    }

    changeTitle = (e) => {
        this.setState({
          'titre':'New Tile add'
        })
    }
    changeTitleByParam = (title) => {
      this.setState({
        titre:title
      })
    }
    changeTitleByUsingBind = (title) => {
      this.setState({
        titre:title
      })
    }

    changeTitleByDynamiqueValue = (e) => {
      this.setState({
        titre: e.target.value
      })
    }

    render() { return (
      <div className="App">
        <MyCars title={this.state.titre}></MyCars>
        <button onClick={this.changeTitle}>change title</button>
        <button onClick={() =>this.changeTitleByParam('Title by parameter')}>change title by parameter</button>
        <button onClick={this.changeTitleByUsingBind.bind(this,'Title by bind')}>change title by bind</button>
        <input type='text' placeholder='add new title' onChange= {this.changeTitleByDynamiqueValue} value={this.state.titre}/>
  </div>
    );
  }
}


export default App;
