import React from 'react';

const Car = ({children , color}) => {
const colorInfo = color ? (<p>Couleur:{color}</p>):(<p>Couleur: neant</p>);
    if(children) {
        return (
            <div className="featured" style= {{backgroundColor:'purple',color:'white',width:'400px',margin:'5px auto', padding:'10px'}}>
                <p>Marque:{children}</p>
                {colorInfo}
            </div>
        )
    } else {
        return (
            <div className="featured" style= {{backgroundColor:'purple',color:'white',width:'400px',margin:'5px auto', padding:'10px'}}>
                <p>Pas de data:</p>
            </div>
        )
    }
}
export default Car;