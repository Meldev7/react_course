import React, { Component } from 'react';
import Car from './Car';

class MyCars extends Component{
    noCopy = () =>{
        alert('thanks do not copy this paragraph');
    }

    addStyle = (e) => {

        if(e.target.classList.contains('h1')) {
            e.target.classList.remove('h1');
        } else {
            e.target.classList.add('h1');
        }
    }

    render(){
        return (
            <div className='featured'>
                <h1 onMouseOver={this.addStyle}>{this.props.title}</h1>
                <p onClick={ this.noCopy }> lorem ipsum dolor sita lorem ipsum dolor sita </p>
                <Car color='black'>Mercedes</Car>
                <Car color='gray'>Ford</Car>
                <Car></Car>
        </div>
        )
    }
}
export default MyCars;